$('#deleteModal').on('show.bs.modal', function (event) 
{
    const button = $(event.relatedTarget);
    const holidaydata = button.data('holiday');
    const modal = $(this);
    modal.find('input#holiday-id').val(holidaydata);
    modal.find('form').attr('action', 'holidays/' + holidaydata);
});

$('#cancellModal').on('show.bs.modal', function (event) 
{
    const button = $(event.relatedTarget);
    const holidaydata = button.data('holiday');
    const modal = $(this);
    modal.find('input#holiday-id').val(holidaydata);
    modal.find('form').attr('action', 'holidays/' + holidaydata + '/cancell');
});