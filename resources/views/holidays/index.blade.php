@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">休暇申請</div>

                {{-- ユーザーのみ休暇申請ボタンを表示ここから --}}
                <div class="card-body">
                    @if(\Auth::user()->role_id == 3)
                    <a href="{{ route('holidays.create') }}"><button type="button" class="btn btn-primary" >休暇を申請する</button></a>
                    @endif
                </div>
                {{-- ユーザーのみ休暇申請ボタンを表示ここまで --}}

                <div class="card-body">
                    @include('holidays.show')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection