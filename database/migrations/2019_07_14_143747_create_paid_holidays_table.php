<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_holidays', function (Blueprint $table) {
            
            /**
            * カラム作成
            */
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->integer('given_days');
            $table->integer('used_days');
            $table->timestamps();
            $table->softDeletes();

            /**
            * 外部キー制約
            * ユーザーレコード削除時に同時削除
            */
            $table->foreign('employee_id')
            ->references('id')
            ->on('employees')
            ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_holidays');
    }
}
