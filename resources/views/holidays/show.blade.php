<table class="table">
    <thead>
        <tr>
            <th>取得日</th>
            @unless(\Auth::user()->role_id == 3)
            <th>取得者</th>
            @endunless
            <th>休暇種別</th>
            <th>ステータス</th>
            <th>申請日時</th>
            @unless(\Auth::user()->role_id == 3)
            <th></th>
            <th></th>
            @endunless
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($holidays as $holiday)
        <tr>
            <td>{{ $holiday->request_date }}</td>
            @unless(\Auth::user()->role_id == 3)
            <td>{{ $holiday->employee->employee_name }}</td>
            @endunless
            <td>{{ $holiday->holidayType->holiday_name }}</td>
            <td>{{ $holiday->is_approved ? ($holiday->is_cancelled ? "取消申請中" : "承認済み") : "申請中" }}</td>
            <td>{{ $holiday->created_at }}</td>

            {{-- 管理者権限ユーザーのみ表示ここから --}}

            @unless(\Auth::user()->role_id == 3)

                {{-- 承認ボタン制御ここから --}}

                    {{-- 承認済みならばボタンは非活性とする --}}
                    @if($holiday->is_approved)
                    <td>
                        <button type="button" class="btn btn-primary" data-holiday="{{ $holiday->id }}" data-toggle="modal" data-target="#myModal" disabled>承認済み</button>
                    </td>

                    {{-- 未承認ならばボタンは活性とする --}}
                    @else
                    <td>
                        <form style="display:inline" method="post" action="holidays/{{ $holiday->id }}/approve">
                            @csrf
                            <input type="hidden" id="holiday-id" name="id" value="{{ $holiday->id }}">
                            <button type="submit" class="btn btn-primary">承認する</button>
                        </form>
                    </td>

                    @endif

                {{-- 取消確定ボタン制御ここまで --}}

                    {{-- 取消申請済ならば取消確認ボタンを表示する --}}

                    @if($holiday->is_cancelled)
                    <td>
                        <button type="button" class="btn btn-danger" data-holiday="{{ $holiday->id }}" data-toggle="modal" data-target="#deleteModal">取消確定</button>
                    </td>


                    {{-- 取消未申請ならば取消確認ボタンは表示しない --}}
                    @else
                    @endif

                {{-- 取消確定ボタン制御ここから --}}



                {{-- 承認ボタン制御ここまで --}}


            {{-- 管理者権限ユーザーのみ表示ここまで --}}

            {{-- ユーザー権限ユーザーのみ表示ここから --}}

            @else
                {{-- 削除ボタン制御ここから --}}

                    {{-- すでに承認済みの場合は削除ボタンは表示しない --}}
                    @if($holiday->is_approved)
                    {{-- 未承認の場合は削除ボタンを表示する --}}
                    @else
                        <td>
                            <button type="button" class="btn btn-danger" data-holiday="{{ $holiday->id }}" data-toggle="modal" data-target="#deleteModal">削除</button>
                        </td>
                    @endif
                {{-- 削除ボタン制御ここまで --}}


                {{-- 申請取消ボタン制御ここから --}}
                    {{-- すでに承認済みの場合は申請取消ボタンを表示する --}}
                    @if($holiday->is_approved)

                        {{-- 取消申請が申請済みであればボタンを非活性化する --}}
                        @if($holiday->is_cancelled)
                        <td>
                        <button type="button" class="btn btn-danger" data-holiday="{{ $holiday->id }}" data-toggle="modal" data-target="#cancellModal" disabled>取消申請</button></td>
                        @else
                        {{-- 取消申請が未申請であればボタンを活性化する --}}
                        <td>
                        <button type="button" class="btn btn-danger" data-holiday="{{ $holiday->id }}" data-toggle="modal" data-target="#cancellModal">取消申請</button></td>
                        @endif
                    @else
                    @endif

                {{-- 申請取消ボタン制御ここまで --}}

            @endunless

            {{-- ユーザー権限ユーザーのみ表示ここから --}}
        </tr>
        @endforeach
    </tbody>
</table>
{{ $holidays->links() }}