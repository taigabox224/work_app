<?php

namespace App\Services;

use App\Models\Employee;
use App\Models\PaidHoliday;

class PaidHolidayService
{

    /**
    * 対象従業員の有給休暇関連の値を計算する
    * 
    * @param string $column 合算したい項目のテーブルカラム名
    * @param int $employee_id
    * @return int
    */
    public function sumElementsOfPaidHolidays(string $column, int $employee_id):int
    {
        // 従業員IDに紐づく有給休暇関連レコードの指定カラムの値を1次元配列の形で取得する。
        $data = PaidHoliday::where('employee_id', $employee_id)->pluck($column)->toArray();
        // 取得した値を全て加算する
        $result = array_sum($data);
        return $result;
    }

    /**
    * 対象従業員の有給消化率を計算する
    * 
    * @param int $employee_id
    * @return int
    */
    public function calculateDigestionRate(int $employee_id):int
    {
        $array_remaining_days = PaidHoliday::where('employee_id', $employee_id)->pluck('remaining_days')->toArray();
        $array_granted_days = PaidHoliday::where('employee_id', $employee_id)->pluck('granted_days')->toArray();

        $remaining_days = array_sum($array_remaining_days);
        $granted_days = array_sum($array_granted_days);
        // 有給消化日数を算出する
        $digested_days = $granted_days - $remaining_days;
        // 有給消化率を算出　※計算式　(有給付与数合計 - 有給残数）/ 有給付与数合計 * 100
        $result = $granted_days ? $digested_days / $granted_days * 100 : 0;
        return $result;

    }
}
