<?php

namespace App\Http\Controllers\Holiday;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\HolidayType;
use App\Models\HolidayRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\HolidayRequestNotification;
use App\Http\Requests\HolidayRequestRequest;
use App\Http\Controllers\Controller;

class HolidayRequestController extends Controller
{

    protected $holidayRequest;

    /**
     * コンストラクタインジェクション
     * @param repository $employees
     * @return void
     */
    public function __construct(HolidayRequest $holidayRequest)
    {
        $this->holidayRequest = $holidayRequest;
    }

    /**
     * 休暇申請の初期処理
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 管理者の場合は全ての休暇申請レコードを取得する
        if(\Auth::user()->role_id != 3)
        {
            $holidays = HolidayRequest::orderBy('created_at', 'desc')
                ->simplePaginate(10);
        } 
        else
        {
        // ユーザーの場合は従業員コードに紐づくレコードのみ取得する
            $holidays = HolidayRequest::where('employee_id', \Auth::user()
                ->employee->id)->orderBy('created_at', 'desc')->simplePaginate(10);
        }

        return view('holidays.index', ['holidays' => $holidays]);
    }

    /**
     * 休暇申請の表示処理
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // プルダウンリストを変数に格納する
        $options = HolidayType::all();
        return view('holidays.create',['options' => $options]);
    }

    /**
     * 休暇申請の新規作成処理
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HolidayRequestRequest $request)
    {
        $holiday = new HolidayRequest;
        $holiday->fill($request->all());

        // 現在ログイン中のユーザーに紐付く従業員IDを取得する
        $holiday->employee_id = \Auth::user()->employee->id;
        $holiday->save();

        // 申請報告メールを管理者に送信する
        $this->sendRequestMail($holiday);

        // リダイレクト処理
        return redirect('holidays')->with('success_message', '申請が完了しました');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 休暇申請の削除処理
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HolidayRequest::find($id)->delete();
        return redirect('holidays')->with('success_message', '削除が完了しました');
    }

    /**
     * 休暇取得承認処理
     *
     * @param  int  $id
     * @return void
     */
    public function approve($id)
    {
        // 休暇申請済みフラグをtrueに切り替える
        HolidayRequest::find($id)->update(['is_approved' => true]);

        // リダイレクト処理
        return redirect('holidays')->with('success_message', '休暇の承認処理が行われました');

    }

    /**
     * 休暇取消申請処理
     *
     * @param  int  $id
     * @return void
     */
    public function cancell($id)
    {
        // 休暇取消申請済みフラグをtrueに切り替える
        HolidayRequest::find($id)->update(['is_cancelled' => true]);

        // リダイレクト処理を行う
        return redirect('holidays')->with('success_message', '休暇取得の取消申請処理が行われました');

    }

    /**
     * 休暇申請報告メールの送信
     *
     * @param  int  $id
     * @return void
     */
    public function sendRequestMail(object $data)
    {
        $request_employee = $data->employee->employee_name;
        $formated_date = date('Y年m月d日', strtotime($data->request_date));

        // メール本文を作成する
        $text  = "以下内容の休暇申請が送信されました。確認をお願い致します。\n\n";
        $text .= "申請者：" . $data->employee->employee_name . "\n";
        $text .= "申請日：" . $formated_date . "\n";
        $text .= "休暇種別：" . $data->holidayType->holiday_name . "\n\n";
        $text .= "よろしくお願い致します。";

        // 改行コードを変換する
        $text = nl2br($text);

        // 送信先の定義
        $to = 't.adachi.carecon@gmail.com'; // テスト用として自分のアドレスを使用

        // mailable を使ってメール送信を行う
        Mail::to($to)->send(new HolidayRequestNotification($request_employee, $text));
    } 
}
