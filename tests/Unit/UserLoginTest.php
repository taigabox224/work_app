<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class UserLoginTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * 認証前にURLから直接アクセスを試みた場合はログイン画面へリダイレクトする
     *
     * @return void
     */
    public function testDirectAccessFromURL()
    {
        $response = $this->get('home');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    /**
     * 正しい情報でログインされた場合、ホーム画面へ遷移するか確認する
     *
     * @return void
     */
    public function testUserLogin()
    {

        // テスト用ユーザーインスタンスを作成
        $user = factory(User::class)->create([
            'password'  => bcrypt('pass')
        ]);
 
        // まだ、認証されていないことを確認する
        $this->assertFalse(\Auth::check());
     
        // // ログインを実行する
        $response = $this->post('login', [
            'email'    => $user->email,
            'password' => 'pass'
        ]);
     
        // // 認証されていることを確認する
        $this->assertTrue(\Auth::check());
     
        // // ログイン後にホームページにリダイレクトされるのを確認
        $response->assertRedirect('home');

    }


    /**
     * 誤った情報でログインされた場合、ログイン画面へリダイレクトされるか確認する
     *
     * @return void
     */
    public function testInvalidLogin()
    {
        // ユーザーを１つ作成
        $user = factory(User::class)->create([
            'password'  => bcrypt('pass')
        ]);
     
        // まだ、認証されていないことを確認
        $this->assertFalse(\Auth::check());
     
        // 異なるパスワードでログインを実行
        $response = $this->post('login', [
            'email'    => $user->email,
            'password' => 'invalid_pass'
        ]);
     
        // 認証失敗で、認証されていないことを確認
        $this->assertFalse(\Auth::check());
     
        // セッションにエラーを含むことを確認
        $response->assertSessionHasErrors(['email']);
     
        // エラメッセージを確認
        $this->assertEquals('These credentials do not match our records.',
            session('errors')->first('email'));
    }
}
