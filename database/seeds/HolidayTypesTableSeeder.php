<?php

use Illuminate\Database\Seeder;

class HolidayTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            ['有給休暇'],
            ['欠勤'],
            ['特別休暇'],
        ];

        foreach($options as $option)
        {
            DB::table('holiday_types')->insert([
                'holiday_name' => $option[0],
            ]);
        }
    }
}
