<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $hashed_password = Hash::make('pass');

        $users = [
            ['システム管理者', 'system@mail.org', $hashed_password, 1],
            ['管理者', 'admin@mail.org', $hashed_password, 2],
            ['一般ユーザー', 'user@mail.org', $hashed_password, 3],
            ['テストユーザー１', 'testuser1@mail.org', $hashed_password, 3],
            ['テストユーザー２', 'testuser2@mail.org', $hashed_password, 3],
            ['テストユーザー３', 'testuser3@mail.org', $hashed_password, 3],
        ];

        foreach($users as $user)
        {
            DB::table('users')->insert([
                'user_name' => $user[0],
                'email' => $user[1],
                'password' => $user[2],
                'role_id' => $user[3]
            ]);
        }
    }
}
