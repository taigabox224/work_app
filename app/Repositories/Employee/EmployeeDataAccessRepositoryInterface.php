<?php

namespace App\Repositories\Employee;

interface EmployeeDataAccessRepositoryInterface
{
    /**
    * 全ての従業員レコードを取得する
    * @return $collection
    */
    public function getAllRecords():object;
}