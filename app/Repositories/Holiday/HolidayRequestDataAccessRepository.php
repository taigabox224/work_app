<?php

namespace App\Repositories\Holiday;

use App\Models\HolidayRequest;

class HolidayRequestDataAccessRepository implements HolidayRequestDataAccessRepositoryInterface
{
    /**
     * メンバ変数
     */
    protected $holidayRequest;

    /**
     * コンストラクタインジェクション
     * @param repository $employees
     * @return void
     */
    public function __construct(HolidayRequest $holidayRequest)
    {
        $this->holidayRequest = $holidayRequest;
    }

    /**
    * 全ての従業員レコードを取得する
    * @return $collection
    */
    public function getAllRecords():object
    {
        return $this->holidayRequest->all();
    }

    /**
    * 対象の従業員の全ての休暇申請レコードを取得する
    * @param int $employee_id
    * @return $collection
    */
    public function getRecordByEmployeeId(int $employee_id):object
    {
        return HolidayRequest::where('employee_id', $employee_id)
        ->orderBy('created_at', 'desc')->simplePaginate(10);
    }

    /**
    * 対象の休暇申請レコードを取得する
    * @param int $id
    * @return $collection
    */
    public function getRecordById(int $id):object
    {
        return HolidayRequest::find($id)->first();
    }

    public function test()
    {
        echo "this is test";
    }
}