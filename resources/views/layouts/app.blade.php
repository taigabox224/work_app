<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

{{-- ヘッダーここから --}}
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRFトークン --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>諸々管理システム</title>

    {{-- スクリプト --}}
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/modal.js') }}" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    {{-- フォント --}}
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    {{-- スタイル --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
{{-- ヘッダーここまで --}}

{{--ボディ部分ここから--}}
<body>

    {{-- トースターメッセージ出力ここから --}}
    <script>
        @if (session('success_message'))
            $(function (){toastr.success('{{ session('success_message') }}');});
        @endif
    </script>
    {{-- トースターメッセージ出力ここまで --}}

    {{-- モーダル出力ここから --}}
    @include('components.delete_modal')
    @include('components.cancell_modal')
    {{-- モーダル出力ここまで --}}

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">業務管理システム</a>
                @include('components.menubar')
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
{{--ボディ部分ここまで--}}
</html>
