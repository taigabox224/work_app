<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            ['営業'],
            ['SES'],
            ['派遣'],
            ['事務'],
            ['カリキュラム生'],
        ];

        foreach($departments as $department)
        {
            DB::table('departments')->insert([
                'department_name' => $department[0],
            ]);
        }
    }
}
