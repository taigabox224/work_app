<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/employee', 'employeeController@testFunc');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('holidays', 'Holiday\HolidayRequestController');
Route::post('holidays/{id}/approve', 'Holiday\HolidayRequestController@approve')->name('approve');
Route::post('holidays/{id}/cancell', 'Holiday\HolidayRequestController@cancell')->name('cancell');
Auth::routes();

Route::get('holidays/mailable/preview', function () {
  return new App\Mail\HolidayRequestNotification();
});

Route::get('holidays/index', 'Holiday\HolidayRequestController@index');
Route::get('holidays/mailable/send', 'Holiday\HolidayRequestController@sendRequestMail');