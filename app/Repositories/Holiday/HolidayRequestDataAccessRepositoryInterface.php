<?php

namespace App\Repositories\Holiday;

interface HolidayRequestDataAccessRepositoryInterface
{
    /**
    * 全ての休暇申請レコードを取得する
    * @return $collection
    */
    public function getAllRecords():object;

    /**
    * 対象の従業員の休暇申請レコードを取得する
    * @return $collection
    */
    public function getAllRecords(int $user_id):object;

    /**
    * 対象の休暇申請レコードを取得する
    * @param int $id
    * @return $collection
    */
    public function getRecordById(int $id):object;
}