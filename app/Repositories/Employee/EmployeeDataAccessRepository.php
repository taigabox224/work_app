<?php

namespace App\Repositories\Employee;

use App\Models\Employee;

class EmployeeDataAccessRepository implements EmployeeDataAccessRepositoryInterface
{
    /**
     * メンバ変数
     */
    protected $employee;

    /**
     * コンストラクタインジェクション
     * @param repository $employees
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
    * 全ての従業員レコードを取得する
    * @return $collection
    */
    public function getAllRecords():object
    {
        return $this->employee->simplePaginate(15);
    }
}