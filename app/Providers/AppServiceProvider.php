<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // EmployeeDataAccessRepository
        $this->app->bind(
            \App\Repositories\Employee\EmployeeDataAccessRepositoryInterface::class,
            \App\Repositories\Employee\EmployeeDataAccessRepository::class
        );

        // HolidayRequestDataAccessRepository
        $this->app->bind(
            \App\Repositories\Holiday\HolidayRequestDataAccessRepositoryInterface::class,
            \App\Repositories\Holiday\HolidayRequestDataAccessRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
