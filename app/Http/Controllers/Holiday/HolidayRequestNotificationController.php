<?php

namespace App\Http\Controllers\Holiday;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\HolidayRequestNotification;

class HolidayRequestNotificationController extends Controller
{
    public function index()
    {
        $name = 'ララベル太郎';
        $text = 'これからもよろしくお願いいたします。';
        $to = 'taigabox@gmail.com';
        Mail::to($to)->send(new HolidayRequestNotification($name, $text));
    }
}
