<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Department;
use App\Services\PaidHolidayService;

class Employee extends Model
{

    protected $guarded = ['id'];

    /**
    * 有給休暇モデルへのリレーション定義
    * @return instance
    */
    public function paidHolidays()
    {
        return $this->hasMany('App\Models\PaidHoliday');
    }

    /**
    * 部署モデルへのリレーション定義
    * @return instance
    */
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    /**
    * 《従業員コード接頭辞》と《従業員コード番号》を結合した《従業員コード》を取得するアクセサ
    * @return string
    */
    public function getFullEmployeeCodeAttribute():string
    {
        return "{$this->employee_code_prefix}{$this->employee_code_number}";
    }

    /**
    * 引数で指定した従業員に紐づく部署名を取得する
    * @return string
    */
    public function getDepartmentName():string
    {
        return Department::find($this->department_id)->department_name;
    }

    // /**
    // * 対象従業員の有給残数を取得する
    // *
    // * @param int $employee_id
    // * @return int
    // */
    // public function getRemainingPaidHolidays(int $employee_id):int
    // {
    //     return $this->paidHolidayService->sumElementsOfPaidHolidays("remaining_days", $employee_id);
    // }

    // /**
    // * 対象従業員の合計有給付与数を取得する
    // *
    // * @param int $employee_id
    // * @return int
    // */
    // public function getTotalPaidHolidays(int $employee_id):int
    // {
    //     return $this->paidHolidayService->sumElementsOfPaidHolidays("granted_days", $employee_id);
    // }

    // /**
    // * 対象従業員の有給消化率を取得する
    // *
    // * @param int $employee_id
    // * @return int
    // */
    // public function getDigestionRate(int $employee_id):int
    // {
    //     return $this->paidHolidayService->calculateDigestionRate($employee_id);
    // }
}
