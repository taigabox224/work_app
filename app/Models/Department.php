<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * メンバ変数
     */
    protected $guarded = ['id'];
}
