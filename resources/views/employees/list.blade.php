<table>
    <caption>従業員の一覧テーブルです</caption>
    <thead>
        <tr>
            <th>従業員コード</th>
            <th>従業員名</th>
            <th>従業員名（かな）</th>
            <th>所属部署</th>
            <th>雇用日</th>
        </tr>
    </thead>
    <tbody>
        @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->full_employee_code }}</td>
            <td>{{ $employee->employee_name }}</td>
            <td>{{ $employee->employee_name_kana }}</td>
            <td>{{ $employee->department->department_name }}</td>
            <td>{{ $employee->hire_date }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $employees->links() }}
