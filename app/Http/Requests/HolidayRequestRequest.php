<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Models\HolidayRequest;

class HolidayRequestRequest extends FormRequest
{
    /**
     * ユーザーがこのリクエストの権限を持っているかを判断する
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * バリデーションルールの定義
     *
     * @return array
     */
    public function rules()
    {
        $date = Carbon::today();

        return [
            'request_date' => 'required | after:now',
            'holiday_type_id' => 'required'
        ];
    }

    /**
     * 同一ユーザーによる同一日付の休暇申請をエラーとするカスタムバリデーション
     *
     * @return void
     */
    public function withValidator($validator)
    {
        $data = HolidayRequest::where('employee_id', \Auth::user()->employee->id)
        ->pluck('request_date')->toArray();

        $isRequested = in_array($this->input('request_date'), $data);

        $validator->after(function ($validator) use ($isRequested)
        {
            if($isRequested) $validator->errors()->add('request_date', 'この日付は既に休暇申請済みです');
        });
    }

    /**
     * 定義済みバリデーションルールのエラーメッセージ取得
     *
     * @return array
     */
    public function messages()
    {
        return [
            'request_date.after' => '休暇申請日に過去の日付が入力されています',
        ];
    }
}
