<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            
            /**
            * カラム作成
            */
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('employee_code_number')->unique();
            $table->string('employee_code_prefix', 255);
            $table->string('employee_name', 255);
            $table->string('employee_name_kana', 255);
            $table->unsignedInteger('department_id');
            $table->date('hire_date');
            $table->timestamps();
            $table->softDeletes();

            /**
            * 外部キー制約
            * 部署テーブルの関連レコード削除を認めない
            */
            $table->foreign('department_id')
            ->references('id')
            ->on('departments')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
