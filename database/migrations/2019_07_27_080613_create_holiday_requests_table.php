<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_requests', function (Blueprint $table) {
            
            /**
            * カラム作成
            */
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('holiday_type_id');
            $table->date('request_date');
            $table->boolean('is_approved')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();

            /**
            * 外部キー制約①
            * 休暇マスタテーブルの関連レコード削除を認めない
            */
            $table->foreign('holiday_type_id')
            ->references('id')
            ->on('holiday_types')
            ->onDelete('restrict'); 
            
            /**
            * 外部キー制約②
            * ユーザーレコード削除時に同時削除
            */
            $table->foreign('employee_id')
            ->references('id')
            ->on('employees')
            ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holiday_requests');
    }
}
