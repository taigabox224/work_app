<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\PaidHolidayService;

class PaidHolidayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(PaidHolidayService::class, function ($app) {
            return new PaidHolidayService();
        });
    }
}
