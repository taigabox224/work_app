<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HolidayRequest extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
    * 休暇種別モデルへのリレーション定義
    * @return instance
    */
    public function holidayType()
    {
        return $this->belongsTo('App\Models\HolidayType');
    }

    /**
    * 従業員モデルへのリレーション定義
    * @return instance
    */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}
