@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">新規作成</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- 申請フォームここから --}}

                    <form method="POST" action="{{ route('holidays.store') }}">
                        @csrf

                        {{-- 休暇取得日ここから --}}

                        <div class="form-group row">
                            <label for="request_date" class="col-md-4 col-form-label text-md-right">取得日</label>

                            <div class="col-md-6">
                                <input id="request_date" type="date" class="form-control @error('request_date') is-invalid @enderror" name="request_date" value="{{ old('request_date') }}" required autocomplete="" autofocus>

                                @error('request_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- 休暇取得日ここまで --}}

                        {{-- 休暇種別ここから --}}

                        <div class="form-group row">
                            <label for="holiday_type_id" class="col-md-4 col-form-label text-md-right">種別</label>

                            <div class="col-md-6">
                                <select id="holiday_type_id" class="form-control @error('holiday_type_id') is-invalid @enderror" name="holiday_type_id" value="{{ old('holiday_type_id') }}" required autocomplete="holiday_type_id">

                                @foreach($options as $option)
                                <option value="{{ $option->id }}">{{ $option->holiday_name }}</option>
                                @endforeach

                                </select>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- 休暇種別ここまで --}}

                        {{-- 申請ボタンここから --}}
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    申請する
                                </button>
                            </div>
                        </div>

                        {{-- 申請ボタンここまで --}}

                    </form>

                    {{-- 申請フォームここまで --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection