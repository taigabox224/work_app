
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">本当に削除しますか？</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="閉じる">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- ToDo:モーダル内に取り消しする申請の詳細情報を載せたい。 --}}
        <p>一度削除した申請は元には戻せません</p>
        <p>必要があれば再度申請してください。</p>
        <div class="holiday"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
        <form style="display:inline" method="post">
            @csrf
            @method('DELETE')
            <input type="hidden" id="holiday-id" name="id" value="">
            <button type="submit" class="btn btn-danger">削除する</button>
        </form>
      </div>
    </div>
  </div>
</div>





